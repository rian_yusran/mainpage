# Rian's Project

- [ ] [Convolution Neural Network](https://gitlab.com/rian_yusran/cnn-python/-/blob/main/cnn_python.py)
```
This is python script using Convolution Neural Network.
We use mnist data.
We classify images into ten classes: 0,1,2,...,9
```

- [ ] [Classification using Random Forest](https://gitlab.com/rian_yusran/python_code/-/blob/main/RandomForest.py)
```
Classification using Random Forest
```

- [ ] [Linear Regression](https://gitlab.com/rian_yusran/python_code/-/blob/main/LinearRegression.py)
```
Linear Regression to predict House Price
```

- [ ] [NLP](https://www.python.org)
```
Natural Language Processing
```

## .: An Expert in Algorithm :.